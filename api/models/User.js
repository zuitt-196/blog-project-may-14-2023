const mongoose = require('mongoose');


const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "must be add username"],
        unique: true
    },
    email: {
        type: String,
        required: [true, "Must be add email"],
        unique: true
    },
    password: {
        type: String,
        required: [true, " must be add password"],
        unique: true
    },
    profile: {
        type: String,
        default: "",
    },


}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema)