const router = require('express').Router();
const User = require('../models/User');
const Post = require('../models/Post');
const bcrypt = require('bcrypt');

// UPDATE SECTION API
router.put("/updateUser/:id", async (req, res) => {
    // compare the urlID and params 
    if (req.body.userId === req.params.id) {
        // if you want to change or update the password but we need to bcrypt
        if (req.body.password) {
            const salt = await bcrypt.genSalt(10);
            req.body.password = await bcrypt.hash(req.body.password, salt);

            try {
                const updatedUser = await User.findByIdAndUpdate(req.params.id, {
                    $set: req.body, // $set it is mongo update operator
                }, { new: true }); // must be add since mongooes has update to the post man and ui will show the update 

                // save and show the client
                res.status(200).json(updatedUser);

            } catch (err) {
                res.status(500).json({ message: err.message });
            }

        } else {
            res.status(401).json({ message: 'You can update only your account' })
        }
    }
})




// DELETE  SECTIION API 
router.delete("/deleteUser/:id", async (req, res) => {
    // comparing the id between the body and params
    if (req.body.userId === req.params.id) {
        try {
            // findung the  user id 
            const user = await User.findById(req.params.id);
            console.log("username:", user.username)
            try {
                await Post.deleteMany({ username: user.username });
                await User.findByIdAndDelete(req.params.id);
                res.status(200).json("User has been deleted...");
            } catch (err) {
                res.status(500).json({ message: err.message });
            }
        } catch (err) {
            res.status(404).json({ message: err.message }, "User not found!");
        }
    } else {
        res.status(401).json("You can delete only your account!");
    }
});



// GET SECTION API 


router.get("/getuserDetails/:id", async (req, res) => {
    // res.send("test")
    try {
        // finding the user with url params.id
        const user = await User.findById(req.params.id);
        // desfracture  the user for the getting the specific details and also the security 
        const { password, ...others } = user._doc;
        // console.log("others:", others);
        res.status(500).json(others);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
})



module.exports = router;