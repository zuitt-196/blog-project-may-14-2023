const router = require('express').Router();
const Category = require('../models/Category')

//  CREATE CATEGORY  SECTION API 
router.post("/createCategory", async (req, res) => {
    const newCat = await new Category(req.body);
    console.log(newCat);
    try {
        const saveCategory = await newCat.save();
        console.log("saveCategor:", saveCategory);
        res.status(200).json(saveCategory);
    } catch (error) {
        res.status(500).json({ error: error });
    }
})


// GET CATEGORY SECTION API

router.get('/getCategory', async (req, res) => {
    try {
        const cats = await Category.find();
        res.status(200).json(cats);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }

})

module.exports = router;