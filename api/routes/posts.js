const router = require('express').Router();
const User = require('../models/User');
const Post = require('../models/Post');
const mongoose = require("mongoose");




//  CREATEP SECTION API 
router.post("/createPost", async (req, res) => {
    const newPost = await new Post(req.body);

    try {
        const savedPost = await newPost.save();
        console.log *
            res.status(200).json(savedPost);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }

})


// UPDATE SECTIION API 
router.put("/postUpdate/:id", async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        //  conditon and compairing usenmae data from id of post model and feild of username 
        if (post.username === req.body.username) {

            try {
                const updatedPost = await Post.findByIdAndUpdate(
                    req.params.id,
                    {
                        $set: req.body,
                    },
                    { new: true }
                );
                // console.log(updatedPost)
                res.status(200).json(updatedPost);
            } catch (err) {
                res.status(500).json(err);
            }
        } else {

            res.status(401).json("You can update only your post! such as Title and Description");
        }
    } catch (err) {
        res.status(500).json(err);
    }
});



// DELETE SECTION API 
router.delete("/postDelete/:id", async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);
        //  conditon and compairing usenmae data from id of post model and feild of username 

        console.log(post.username === req.body.username);
        if (post.username === req.body.username) {
            try {
                await post.deleteOne();
                res.status(200).json("post Has been deleted..");

            } catch (err) {
                res.status(500).json(err);
            }
        } else {

            res.status(401).json("You can delete only your post! such as Title and Description");
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
})



// GET POST SECTION API

router.get("/getPostById/:id", async (req, res) => {

    try {
        const post = await Post.findById(req.params.id);
        res.status(200).json(post);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }

});

// GET ALL POST API
router.get('/getAllpost/', async (req, res) => {
    // DEFINE THE QUERY OF URL
    const user = req.query.user;
    const catName = req.query.cat;
    try {
        let posts;
        if (user) {
            posts = await Post.find({ username: user })
        } else if (catName) {
            posts = await Post.find({
                categories: {
                    $in: [catName]
                }
            })
        } else {
            posts = await Post.find();
        }
        res.status(200).json(posts);
    } catch (error) {
        console.log("error")
        res.status(500).json({ message: error.message });
    }
})



module.exports = router;