const router = require('express').Router();
const User = require('../models/User');
const bcrypt = require('bcrypt');


// REGISTER SECTION 
router.post("/register", async (req, res) => {
    // ERROR HANDLING SECTION 
    try {
        const salt = await bcrypt.genSalt(10);
        const hashedPass = await bcrypt.hash(req.body.password, salt);
        const newUser = new User({
            username: req.body.username,
            email: req.body.email,
            password: hashedPass,
        });

        const user = await newUser.save();
        res.status(200).json(user)
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
})



// lOGIN SECTION 
router.post("/login", async (req, res) => {

    try {
        const user = await User.findOne({ username: req.body.username })

  

        if (!user) {
            throw new Error('Wrong credential make it is correct !!')
        }

        const validate = await bcrypt.compare(req.body.password, user.password);

        if (!validate) {
            throw new Error('Wrong credential make it is correct !!')
        }

        // DESFRATURE THE DATA FROM USER 
        const { password, ...others } = user._doc;
        // console.log(others);
        res.status(200).json(others);

    } catch (err) {
        return res.status(500).json({ message: err.message });
    }

})














module.exports = router;